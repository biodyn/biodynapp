package br.unb.biodyn.DAO;

public class TableExerciseData {
	
	static final String TABLE_EXERCISE_DATA = "exercise_data";
	
	static final String COLUMN_NAME = "name";
	static final String COLUMN_SERIES = "series";
	static final String COLUMN_REPETITIONS = "repetitions";
	static final String COLUMN_MAXIMUM_WEIGHT = "maximun_weight";
	static final String COLUMN_MINIMUM_WEIGHT = "minimum_weight";
	static final String COLUMN_REST_TIME = "rest_time";
	static final String COLUMN_HOLD_TIME = "hold_time";
	
	public static final String CREATE_SCRIPT ="create table " + TABLE_EXERCISE_DATA + "(" 
			+ COLUMN_NAME + " STRING PRIMARY KEY" + ", " 
			+ COLUMN_SERIES + " INTEGER" + ", " 
			+ COLUMN_REPETITIONS + " INTEGER" + ", "
			+ COLUMN_MAXIMUM_WEIGHT + " FLOAT" + ", "
			+ COLUMN_MINIMUM_WEIGHT + " FLOAT" + ", "
			+ COLUMN_REST_TIME + " LONG" + ", "
			+ COLUMN_HOLD_TIME + " LONG" +");";
			
	public static final String DROP_SCRIPT= "DROP TABLE "+TABLE_EXERCISE_DATA+";";
}
