package br.unb.biodyn.DAO;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import br.unb.biodyn.model.ExerciseData;

public class ExerciseDAO {

	private static final String TAG = "DAOExercise";
	private static ExerciseDAO instance;
	private ExerciseDataHelper helper;
	private SQLiteDatabase db;
	private String[] allColumns = { TableExerciseData.COLUMN_NAME, TableExerciseData.COLUMN_SERIES,
			TableExerciseData.COLUMN_REPETITIONS, TableExerciseData.COLUMN_MAXIMUM_WEIGHT,
			TableExerciseData.COLUMN_MINIMUM_WEIGHT, TableExerciseData.COLUMN_REST_TIME, TableExerciseData.COLUMN_HOLD_TIME };

	private ExerciseDAO(Context context) {
		helper = new ExerciseDataHelper(context);
	}

	public static synchronized ExerciseDAO getInstance(Context context) {
		if (instance == null)
			instance = new ExerciseDAO(context);
		return instance;
	}

	public void insertExerciseData(String name, ExerciseData exerciseData) throws SQLException {
		if (db == null) {
			Log.e(TAG, "inserting with null db!! aborting...");
			return;
		}

		ContentValues values = new ContentValues();

		values.put(TableExerciseData.COLUMN_MAXIMUM_WEIGHT, exerciseData.getMaximumWeight());
		values.put(TableExerciseData.COLUMN_MINIMUM_WEIGHT, exerciseData.getMinimumWeight());
		values.put(TableExerciseData.COLUMN_NAME, name);
		values.put(TableExerciseData.COLUMN_REPETITIONS, exerciseData.getRepetitions());
		values.put(TableExerciseData.COLUMN_SERIES, exerciseData.getSeries());
		values.put(TableExerciseData.COLUMN_REST_TIME, exerciseData.getRestTime());

		db.insert(TableExerciseData.TABLE_EXERCISE_DATA, null, values);
	}

	public void updateExerciseData(String name, ExerciseData exerciseData) throws SQLException {
		if (db == null) {
			Log.e(TAG, "updating with null db!! aborting...");
			return;
		}

		ContentValues values = new ContentValues();

		values.put(TableExerciseData.COLUMN_MAXIMUM_WEIGHT, exerciseData.getMaximumWeight());
		values.put(TableExerciseData.COLUMN_MINIMUM_WEIGHT, exerciseData.getMinimumWeight());
		values.put(TableExerciseData.COLUMN_NAME, name);
		values.put(TableExerciseData.COLUMN_REPETITIONS, exerciseData.getRepetitions());
		values.put(TableExerciseData.COLUMN_SERIES, exerciseData.getSeries());
		values.put(TableExerciseData.COLUMN_REST_TIME, exerciseData.getRestTime());
		values.put(TableExerciseData.COLUMN_HOLD_TIME, exerciseData.getHoldTime());

		db.update(TableExerciseData.TABLE_EXERCISE_DATA, values, TableExerciseData.COLUMN_NAME + "=?",
				new String[] { name });
	}

	public void deleteExerciseData(String name) throws SQLException {

		if (db == null) {
			Log.e(TAG, "deleting with null db!! aborting...");
			return;
		}

		db.delete(TableExerciseData.TABLE_EXERCISE_DATA, TableExerciseData.COLUMN_NAME + "=?", new String[] { name });
	}

	public ExerciseData readExerciseData(String name) throws SQLException {

		if (db == null) {
			Log.e(TAG, "reading from null db!! aborting...");
			return null;
		}

		Cursor cursor = db.query(TableExerciseData.TABLE_EXERCISE_DATA, allColumns, TableExerciseData.COLUMN_NAME
				+ "=?", new String[] { name }, null, null, null);

		ExerciseData exerciseData = null;

		if (cursor.moveToFirst())
			exerciseData = cursorToExerciseData(cursor);

		cursor.close();
		return exerciseData;
	}

	private ExerciseData cursorToExerciseData(Cursor cursor) {
		float maximumWeight = cursor.getFloat(cursor.getColumnIndex(TableExerciseData.COLUMN_MAXIMUM_WEIGHT));
		float minimumWeight = cursor.getFloat(cursor.getColumnIndex(TableExerciseData.COLUMN_MINIMUM_WEIGHT));
		int repetitions = cursor.getInt(cursor.getColumnIndex(TableExerciseData.COLUMN_REPETITIONS));
		long restTime = cursor.getLong(cursor.getColumnIndex(TableExerciseData.COLUMN_REST_TIME));
		int series = cursor.getInt(cursor.getColumnIndex(TableExerciseData.COLUMN_SERIES));
		String name = cursor.getString(cursor.getColumnIndex(TableExerciseData.COLUMN_NAME));
		long holdTime = cursor.getLong(cursor.getColumnIndex(TableExerciseData.COLUMN_HOLD_TIME));
		
		return new ExerciseData(name, maximumWeight, minimumWeight, repetitions, series, restTime, holdTime);
	}

	public List<ExerciseData> readAll() {

		List<ExerciseData> exerciseDataList = new ArrayList<ExerciseData>();

		if (db == null) {
			Log.e(TAG, "reading from null db!! aborting...");
			return null;
		}

		Cursor cursor = db.query(TableExerciseData.TABLE_EXERCISE_DATA, allColumns, null, null, null, null, null);

		if (cursor.moveToFirst()) {
			ExerciseData exerciseData = cursorToExerciseData(cursor);
			exerciseDataList.add(exerciseData);

			while (cursor.moveToNext()) {
				exerciseData = cursorToExerciseData(cursor);
				exerciseDataList.add(exerciseData);
			}
		}

		return exerciseDataList;

	}

	public void open() throws SQLException {
		synchronized (instance) {
			db = helper.getWritableDatabase();
		}
	}

	public void close() {
		db.close();
		//helper.close();
	}
}
