package br.unb.biodyn.model;

import android.util.Log;
import br.unb.biodyn.Communicator;

public abstract class Exercise extends ExerciseData implements ExerciseInterface {

	public static final String TAG = "Exercise";
	private boolean calledBioFeedback = false;
	
	public Exercise(ExerciseData data) {
		super(data.getMaximumWeight(), data.getMinimumWeight(), data
				.getRepetitions(), data.getSeries(),data.getRestTime(), data.getHoldTime());
		reset();
	}

	public void computeData(float data) {
		
		Log.d(TAG, "ComputeData: " + data);
		if (checkRepetition(data))
			countDownRepetitions();
		checkMinimumBioFeedback(data);
		checkMaximumBioFeedback(data);
	}
	
	public void checkMinimumBioFeedback (float realWeight) {
		
		Log.d(TAG, "Value of realWeight in checkMinimumRange(): " 
				+ realWeight);
		
		Log.d(TAG, "Value of minimumWeight in checkMinimumRange(): "
				+ minimumWeight);
		
	

		if (realWeight < minimumWeight) {
			
			Log.d(TAG, "Sending feedback that is below minimumWeight.");

			Communicator
					.getInstance()
					.sendToBioFeedback(
							Communicator.BioFeedbackServiceActions.ACTION_START,
							Communicator.BioFeedbackServiceActions.FLAG_VIBRATION,
							1000);
			calledBioFeedback = true;
		} else {
			calledBioFeedback = false;
		}
	}
	
	private void checkMaximumBioFeedback (float realWeight) {

		Log.d(TAG, "maximumWeight: " + maximumWeight);

		if (realWeight > maximumWeight) {

			Log.d(TAG, "Sending feedback that is above maximumWeight.");

			Communicator
					.getInstance()
					.sendToBioFeedback(
							Communicator.BioFeedbackServiceActions.ACTION_START,
							Communicator.BioFeedbackServiceActions.FLAG_VIBRATION,
							1000);
			calledBioFeedback = true;
		} else {
			calledBioFeedback = false;
		}

	} 
}
