package br.unb.biodyn.model;

import android.os.CountDownTimer;
import android.util.Log;
import br.unb.biodyn.Communicator;

public class DynamicExercise extends Exercise {

	/********************************** CONSTANTS ***********************************/
	
	public static final String TAG = "DynamicExercise";
	private static final float PERCENT_ERROR_MAXIMUM = 0.9f;
	private static final float PERCENT_ERROR_MINIMUM = 1.1f;
	private static final long CONFIRM_TIMER_DURATION = 4000;

	private static final float DEFAULT_MAXIMUM_WEIGHT = 10;
	private static final float DEFAULT_MINIMUM_WEIGHT = 2;
	private static final int DEFAULT_REPETITIONS = 10;
	private static final int DEFAULT_SERIES = 5;
	private static final int DEFAULT_REST_TIME = 30000;
	private static final int DEFAULT_HOLD_TIME = 0;

	/********************************** CLASS FIELDS ***********************************/

	private boolean wasFirstMinimumWeight = false;
	private boolean wasMaximumWeight = false;
	private ConfirmTimer myConfirmTimer;
	private boolean restTime = false;
	private float realWeight;

	/********************************** CLASS METHODS ***********************************/

	public DynamicExercise(ExerciseData data) {
		super(data);
	}

	public DynamicExercise() {
		this(new ExerciseData(DEFAULT_MAXIMUM_WEIGHT, DEFAULT_MINIMUM_WEIGHT,
				DEFAULT_REPETITIONS, DEFAULT_SERIES, DEFAULT_REST_TIME, DEFAULT_HOLD_TIME));

	}

	private void checkFirstMinimumValue() {
		if (realWeight >= minimumWeight) {
			this.wasFirstMinimumWeight = true;
			Log.d("debug", "checkFirstMinimumValue");
		}
	}

	private void checkMaximumRange() {
		if (realWeight >= maximumWeight * PERCENT_ERROR_MAXIMUM) {
			this.wasMaximumWeight = true;
			Log.d("debug", "checkMaximumRange");
		}
	}

	private boolean checkMinimumRange() {
		boolean result = false;

		if (this.wasFirstMinimumWeight == true && this.wasMaximumWeight == true
				&& realWeight <= minimumWeight * PERCENT_ERROR_MINIMUM) {
			this.wasFirstMinimumWeight = false;
			this.wasMaximumWeight = false;
			result = true;
		}

		return result;
	}

	/********************************** INHERITED ABSTRACT METHODS ***********************************/

	@Override
	public boolean checkRepetition(float data) {
		realWeight = data;
		checkFirstMinimumValue();
		checkMaximumRange();
		return checkMinimumRange();
	}

	@Override
	public void reset() {
		wasFirstMinimumWeight = false;
		wasMaximumWeight = false;
		repetitions = this.initialRepetitions;
		series = this.initialSeries;
		Communicator.getInstance().sendToUI(
				Communicator.FLAG_SERIES | Communicator.FLAG_REPETITIONS
						| Communicator.FLAG_LIMITS, this);
		Log.d(TAG, "is reseting the values");
	}

	@Override
	public void countDownRepetitions() {
		repetitions--;

		if (myConfirmTimer != null) {
			myConfirmTimer.cancel();
			myConfirmTimer = null;
		}

		if (myConfirmTimer == null)
			myConfirmTimer = new ConfirmTimer();

		if (restTime) {
			restTime = false;
			Communicator.getInstance().sendToUI(Communicator.FLAG_STOP_TIMER,
					this);
		}

		myConfirmTimer.start();

		// TODO -> Save the repetitions number to show at the end (list)

		Communicator.getInstance()
				.sendToUI(Communicator.FLAG_REPETITIONS, this);
	}

	@Override
	public void countDownSeries() {
		this.series--;

		if (series == 0) {
			reset();
			stop();
		} else {
			Communicator.getInstance().sendToUI(Communicator.FLAG_START_TIMER,
					this);

			restTime = true;
		}
		repetitions = initialRepetitions;

		Communicator.getInstance().sendToUI(
				Communicator.FLAG_SERIES | Communicator.FLAG_REPETITIONS, this);
	}

	@Override
	public void stop() {
		reset();
		Communicator.getInstance().sendToUI(Communicator.FLAG_FINISHED, this);
	}

	/********************************** PRIVATE CLASSES ***********************************/

	private class ConfirmTimer extends CountDownTimer {

		public ConfirmTimer() {
			super(CONFIRM_TIMER_DURATION, CONFIRM_TIMER_DURATION);
		}

		@Override
		public void onTick(long millisUntilFinished) {
		}

		@Override
		public void onFinish() {
			countDownSeries();
			Communicator.getInstance().sendToUI(Communicator.FLAG_START_TIMER, DynamicExercise.this);
		}
	}

}
