package br.unb.biodyn.ui;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import br.unb.biodyn.R;
import br.unb.biodyn.DAO.ExerciseDAO;
import br.unb.biodyn.model.ExerciseData;

public class DeleteItemAdapter extends ArrayAdapter<ExerciseData> implements
		OnClickListener {

	ArrayList<String> nameList = new ArrayList<String>();

	public DeleteItemAdapter(Context context, List<ExerciseData> objects) {
		super(context, R.layout.list_item_delete, objects);
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		ExerciseDAO.getInstance(getContext()).open();
		for (String name : nameList) {
			ExerciseDAO.getInstance(getContext()).deleteExerciseData(name);
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final String nameFromList = getItem(position).toString();

		LayoutInflater layoutInflater = (LayoutInflater) getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = layoutInflater.inflate(R.layout.list_item_delete, parent,
				false);

		CheckBox checkBox = (CheckBox) rowView
				.findViewById(R.id.checkBoxViewItemExerciseList);
		checkBox.setText(getItem(position).toString());

		if (checkBox.isEnabled() && nameList.contains(checkBox.getText())) {
			checkBox.setChecked(true);
		}

		checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked == true) {
					nameList.add(nameFromList);
				} else {
					nameList.remove(nameFromList);
				}
			}
		});

		return rowView;
	}
}
