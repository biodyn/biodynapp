package br.unb.biodyn.ui;

import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import br.unb.biodyn.Communicator;
import br.unb.biodyn.R;
import br.unb.biodyn.DAO.ExerciseDAO;
import br.unb.biodyn.model.ExerciseData;

public class LoadItemAdapter extends ArrayAdapter<ExerciseData> implements
		OnItemClickListener {

	protected String name;
	protected Context context;
	protected AlertDialog alertDialogLoad;

	public LoadItemAdapter(Context context, List<ExerciseData> objects) {
		super(context, R.layout.list_item_delete, objects);
		this.context = context;
	}

	public void setCalledAlertDialog(AlertDialog alertDialog) {
		this.alertDialogLoad = alertDialog;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		name = getItem(arg2).getName();
		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		builder.setTitle("Are you sure?");
		builder.setPositiveButton("OK", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				ExerciseDAO.getInstance(getContext()).open();
				ExerciseData exerciseData = ExerciseDAO.getInstance(
						getContext()).readExerciseData(name);
				Communicator.getInstance().sendToUI(
						Communicator.FLAG_SERIES | Communicator.FLAG_LIMITS
								| Communicator.FLAG_REPETITIONS
								| Communicator.FLAG_REST_TIME, exerciseData);
				Communicator.getInstance().sendToExerciseService(
						Communicator.FLAG_SERIES | Communicator.FLAG_LIMITS
								| Communicator.FLAG_REPETITIONS
								| Communicator.FLAG_REST_TIME, exerciseData);
				ExerciseDAO.getInstance(getContext()).close();
				alertDialogLoad.dismiss();
			}
		});

		builder.setNegativeButton("Cancel", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		AlertDialog alertDialog = builder.create();
		alertDialog.show();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		LayoutInflater layoutInflater = (LayoutInflater) getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = layoutInflater.inflate(R.layout.list_item_load_ui,
				parent, false);
		TextView textView = (TextView) rowView
				.findViewById(R.id.textViewItemExerciseList);
		textView.setText(getItem(position).toString());

		return rowView;
	}
}
