package br.unb.biodyn;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import br.unb.biodyn.model.ExerciseData;

public class Communicator {
	/********************************** CONSTANTS ***********************************/

	public static final String TAG = "Communicator";
	
	public static final int FLAG_SERIES = 0x0001;
	public static final int FLAG_REPETITIONS = 0x0002;
	public static final int FLAG_LIMITS = 0x0004;
	public static final int FLAG_FINISHED = 0x0008;
	public static final int FLAG_STOP_TIMER = 0x0010;
	public static final int FLAG_START_TIMER = 0x0020;
	public static final int FLAG_TOOGLE_RESET = 0x0040;
	public static final int FLAG_FORCE_REPETITION = 0x0080;
	public static final int FLAG_HOLD_TIME = 0x0100;
	public static final int FLAG_START_HOLD_TIME = 0x0200;
	public static final int FLAG_STOP_HOLD_TIME= 0x0400;
	public static final int FLAG_REST_TIME = 0x0800;
	
	public static final String KEY_TOP_LIMIT = "top_limit";
	public static final String KEY_BOTTOM_LIMIT = "bottom_limit";
	public static final String KEY_SERIES = "series";
	public static final String KEY_REPETITIONS = "repetitions";
	public static final String KEY_REST_TIME="rest_time";
	public static final String KEY_HOLD_TIME="hold_time";

	/********************************** CLASS FIELDS ***********************************/

	private static Communicator instance;
	LocalBroadcastManager broadcastManager;

	/********************************** CLASS METHODS ***********************************/

	private Communicator(LocalBroadcastManager broadcastManager) {
		this.broadcastManager = broadcastManager;
	}

	public static void initialize(LocalBroadcastManager lbm) {
		instance = new Communicator(lbm);
	}

	public synchronized static Communicator getInstance() {
		return instance;
	}

	public synchronized void sendToUI(int flags, ExerciseData data) {
		if ((flags & FLAG_SERIES) != 0) {
			Intent intent = new Intent(UIActions.ACTION_UPDATE_SERIES);
			intent.putExtra(KEY_SERIES, data.getSeries());
			broadcastManager.sendBroadcast(intent);
		}
		if ((flags & FLAG_REPETITIONS) != 0) {
			Intent intent = new Intent(UIActions.ACTION_UPDATE_REPETITIONS);
			intent.putExtra(KEY_REPETITIONS, data.getRepetitions());
			broadcastManager.sendBroadcast(intent);
		}
		if ((flags & FLAG_LIMITS) != 0) {
			Intent intent = new Intent(UIActions.ACTION_UPDATE_LIMITS);
			intent.putExtra(KEY_BOTTOM_LIMIT, data.getMinimumWeight());
			intent.putExtra(KEY_TOP_LIMIT, data.getMaximumWeight());
			broadcastManager.sendBroadcast(intent);
		}
		if ((flags & FLAG_FINISHED) != 0) {
			Intent intent = new Intent(UIActions.ACTION_EXERCISE_FINISHED);
			broadcastManager.sendBroadcast(intent);
		}
		if ((flags & FLAG_STOP_TIMER) != 0) {
			Intent intent = new Intent(UIActions.ACTION_STOP_REST_TIMER);
			broadcastManager.sendBroadcast(intent);
		}
		if ((flags & FLAG_START_TIMER) != 0) {
			Intent intent = new Intent(UIActions.ACTION_START_REST_TIMER);
			intent.putExtra(KEY_REST_TIME, data.getRestTime());
			broadcastManager.sendBroadcast(intent);
		}
		if ((flags & FLAG_HOLD_TIME) != 0) {
			Intent intent = new Intent(UIActions.ACTION_UPDATE_HOLD_TIME);
			intent.putExtra(KEY_HOLD_TIME, data.getHoldTime());
			broadcastManager.sendBroadcast(intent);
		}
		if ((flags & FLAG_START_HOLD_TIME) != 0) {
			Intent intent = new Intent(UIActions.ACTION_START_HOLD_TIME);
			intent.putExtra(KEY_HOLD_TIME, data.getHoldTime());
			broadcastManager.sendBroadcast(intent);
		}
		if ((flags & FLAG_STOP_HOLD_TIME) != 0) {
			Intent intent = new Intent(UIActions.ACTION_STOP_HOLD_TIME);
			broadcastManager.sendBroadcast(intent);
		}
	}

	public synchronized void sendToExerciseService(int flags, ExerciseData data) {
		if ((flags & FLAG_TOOGLE_RESET) != 0) {
			Intent intent = new Intent(ExerciseServiceActions.ACTION_TOGGLE_RESET);
			broadcastManager.sendBroadcast(intent);
			return;
		}
		if ((flags & FLAG_SERIES) != 0) {
			Intent intent = new Intent(ExerciseServiceActions.ACTION_UPDATE_SERIES);
			intent.putExtra(KEY_SERIES, data.getSeries());
			broadcastManager.sendBroadcast(intent);
		}
		if ((flags & FLAG_REPETITIONS) != 0) {
			Intent intent = new Intent(ExerciseServiceActions.ACTION_UPDATE_REPETITIONS);
			intent.putExtra(KEY_REPETITIONS, data.getRepetitions());
			broadcastManager.sendBroadcast(intent);
		}
		if ((flags & FLAG_LIMITS) != 0) {
			Intent intent = new Intent(ExerciseServiceActions.ACTION_UPDATE_LIMITS);
			intent.putExtra(KEY_BOTTOM_LIMIT, data.getMinimumWeight());
			intent.putExtra(KEY_TOP_LIMIT, data.getMaximumWeight());
			broadcastManager.sendBroadcast(intent);
			Log.d(TAG,"Getting Flag of the limits");
		}
		if ((flags & FLAG_FORCE_REPETITION) != 0) {
			Intent intent = new Intent(ExerciseServiceActions.ACTION_FORCE_REPETITION);
			broadcastManager.sendBroadcast(intent);
		}
		if ((flags & FLAG_HOLD_TIME) != 0) {
			Intent intent = new Intent(ExerciseServiceActions.ACTION_UPDATE_HOLD_TIME);
			intent.putExtra(KEY_HOLD_TIME, data.getHoldTime());
			broadcastManager.sendBroadcast(intent);
		}
		if((flags & FLAG_REST_TIME) != 0) {
			Intent intent = new Intent(ExerciseServiceActions.ACTION_REST_TIME);
			intent.putExtra(KEY_REST_TIME, data.getRestTime());
			broadcastManager.sendBroadcast(intent);
		}
	}
	
	public synchronized void sendToBioFeedback(String action, int flags, long time) {
		Intent intent = new Intent(action);
		
		Log.d(TAG, "Send flag to bioFeedBack " + flags);

		intent.putExtra(BioFeedbackServiceActions.KEY_FLAGS, flags);
		intent.putExtra(BioFeedbackServiceActions.KEY_BIOFEEDBACK_TIME, time);
		broadcastManager.sendBroadcast(intent);
	}

	/********************************** INTERNAL CLASSES ***********************************/

	public class UIActions {
		public static final String ACTION_START_REST_TIMER = "br.unb.biodyn.Communicator.UIActions.start_rest_timer";
		public static final String ACTION_STOP_REST_TIMER = "br.unb.biodyn.Communicator.UIActions.stop_rest_timer";
		public static final String ACTION_UPDATE_REPETITIONS = "br.unb.biodyn.Communicator.UIActions.update_repetitions";
		public static final String ACTION_UPDATE_SERIES = "br.unb.biodyn.Communicator.UIActions.update_series";
		public static final String ACTION_EXERCISE_FINISHED = "br.unb.biodyn.Communicator.UIActions.finished";
		public static final String ACTION_UPDATE_LIMITS = "br.unb.biodyn.Communicator.UIActions.update_limits";
		public static final String ACTION_UPDATE_HOLD_TIME = "br.unb.biodyn.Communicator.UIActions.update_hold_time";
		public static final String ACTION_START_HOLD_TIME = "br.unb.biodyn.Communicator.UIActions.start_hold_time";
		public static final String ACTION_STOP_HOLD_TIME = "br.unb.biodyn.Communicator.UIActions.stop_hold_time";
		
	}

	public class ExerciseServiceActions {
		public static final String ACTION_TOGGLE_RESET = "br.unb.biodyn.Communicator.ServiceActions.toggle_reset";
		public static final String ACTION_UPDATE_LIMITS = "br.unb.biodyn.Communicator.ServiceActions.update_limits";
		public static final String ACTION_UPDATE_REPETITIONS = "br.unb.biodyn.Communicator.ServiceActions.update_repetitions";
		public static final String ACTION_UPDATE_SERIES = "br.unb.biodyn.Communicator.ServiceActions.update_series";
		public static final String ACTION_UPDATE_HOLD_TIME = "br.unb.biodyn.Communicator.ServiceActions.update_hold_time";
		
		// TEST PURPOSES
		public static final String ACTION_FORCE_REPETITION = "force_repetition";
		public static final String ACTION_REST_TIME = "rest_time";
	}
	
	public class BioFeedbackServiceActions {
		public static final String ACTION_START = "br.unb.biodyn.BioFeedback.Service.start";
		public static final String ACTION_STOP = "br.unb.biodyn.BioFeedback.Service.stop";
		
		public static final int FLAG_VIBRATION = 0x0001;

		public static final String KEY_FLAGS = "key";
		public static final String KEY_BIOFEEDBACK_TIME = "time";
	}
}
