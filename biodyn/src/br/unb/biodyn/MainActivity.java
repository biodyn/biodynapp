package br.unb.biodyn;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteConstraintException;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.NumberPicker.OnValueChangeListener;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import br.unb.biodyn.DAO.ExerciseDAO;
import br.unb.biodyn.model.DynamicExercise;
import br.unb.biodyn.model.ExerciseData;
import br.unb.biodyn.model.IsometricExercise;
import br.unb.biodyn.ui.DeleteItemAdapter;
import br.unb.biodyn.ui.LoadItemAdapter;

public class MainActivity extends Activity {

	/********************************** CONSTANTS ***********************************/

	// Intent request code for result checking
	private static final int REQUEST_CONNECT_DEVICE = 1;
	private static final int REQUEST_ENABLE_BT = 2;
	private static final long DEFAULT_DURATION= 30000;
	
	public static final String TAG = "MainActivity"; 

	/********************************** CLASS FIELDS ***********************************/

	private String mConnectedDeviceName = null;
	private BluetoothAdapter mBluetoothAdapter = null;
	private mReceiver myReceiver;

	private TextView textTimer;
	private RestTimer restTimer;
	private ProgressBar barTimer;

	NumberPicker repetitions_picker;
	NumberPicker series_picker;
	NumberPicker top_limit_picker;
	NumberPicker bottom_limit_picker;

	DynamicExercise dynamicExercise;
	IsometricExercise isometricExercise;

	/********************************** LIFECYCLE METHODS ***********************************/

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "+++ ON CREATE +++");
		setContentView(R.layout.main_screen);

		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		if (mBluetoothAdapter == null) {
			Toast.makeText(this, "Bluetooth is not available on this device!", Toast.LENGTH_LONG).show();
			this.finish();
			return;
		}

		repetitions_picker = (NumberPicker) findViewById(R.id.count_picker);
		repetitions_picker.setMaxValue(30);
		repetitions_picker.setMinValue(1);
		repetitions_picker.setValue(10);
		repetitions_picker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
		repetitions_picker.setOnValueChangedListener(new OnValueChangeListener() {
			@Override
			public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
				ExerciseData exerciseData = new ExerciseData();
				exerciseData.setRepetitions(newVal);
				Communicator.getInstance().sendToExerciseService(Communicator.FLAG_REPETITIONS, exerciseData);
			}
		});

		series_picker = (NumberPicker) findViewById(R.id.series_picker);
		series_picker.setMaxValue(30);
		series_picker.setMinValue(1);
		series_picker.setValue(5);
		series_picker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
		series_picker.setOnValueChangedListener(new OnValueChangeListener() {
			@Override
			public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
				ExerciseData exerciseData = new ExerciseData();
				exerciseData.setSeries(newVal);
				Communicator.getInstance().sendToExerciseService(Communicator.FLAG_SERIES, exerciseData);
			}
		});

		top_limit_picker = (NumberPicker) findViewById(R.id.top_limit_picker);
		top_limit_picker.setMaxValue(30);
		top_limit_picker.setMinValue(1);
		top_limit_picker.setValue(8);
		top_limit_picker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
		top_limit_picker.setOnValueChangedListener(new OnValueChangeListener() {
			@Override
			public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
				ExerciseData exerciseData = new ExerciseData();
				exerciseData.setMaximumWeight(newVal);
				exerciseData.setMinimumWeight(bottom_limit_picker.getValue());
				Communicator.getInstance().sendToExerciseService(Communicator.FLAG_LIMITS, exerciseData);
			}
		});

		bottom_limit_picker = (NumberPicker) findViewById(R.id.bottom_limit_picker);
		bottom_limit_picker.setMaxValue(30);
		bottom_limit_picker.setMinValue(1);
		bottom_limit_picker.setValue(2);
		bottom_limit_picker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
		bottom_limit_picker.setOnValueChangedListener(new OnValueChangeListener() {
			@Override
			public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
				ExerciseData exerciseData = new ExerciseData();
				exerciseData.setMaximumWeight(top_limit_picker.getValue());
				exerciseData.setMinimumWeight(newVal);
				Communicator.getInstance().sendToExerciseService(Communicator.FLAG_LIMITS, exerciseData);
			}
		});

		textTimer = (TextView) findViewById(R.id.text_timer);
		barTimer = (ProgressBar) findViewById(R.id.progressBarToday);

		textTimer.setText("STOP");
		barTimer.setProgress(100);

		myReceiver = new mReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(BluetoothService.ACTION_DATA_RECEIVED);
		filter.addAction(BluetoothService.ACTION_MESSAGE_DEVICE_NAME);
		filter.addAction(BluetoothService.ACTION_MESSAGE_SHOW_TOAST);
		filter.addAction(Communicator.UIActions.ACTION_EXERCISE_FINISHED);
		filter.addAction(Communicator.UIActions.ACTION_START_REST_TIMER);
		filter.addAction(Communicator.UIActions.ACTION_STOP_REST_TIMER);
		filter.addAction(Communicator.UIActions.ACTION_UPDATE_REPETITIONS);
		filter.addAction(Communicator.UIActions.ACTION_UPDATE_SERIES);
		filter.addAction(Communicator.UIActions.ACTION_UPDATE_LIMITS);
		filter.addAction(Communicator.UIActions.ACTION_UPDATE_HOLD_TIME);
		filter.addAction(Communicator.UIActions.ACTION_START_HOLD_TIME);
		filter.addAction(Communicator.UIActions.ACTION_STOP_HOLD_TIME);
		filter.addAction(Communicator.ExerciseServiceActions.ACTION_UPDATE_LIMITS);
		filter.addAction(Communicator.ExerciseServiceActions.ACTION_TOGGLE_RESET);
		filter.addAction(Communicator.ExerciseServiceActions.ACTION_UPDATE_REPETITIONS);
		filter.addAction(Communicator.ExerciseServiceActions.ACTION_UPDATE_SERIES);
		filter.addAction(Communicator.ExerciseServiceActions.ACTION_UPDATE_HOLD_TIME);
		filter.addAction(Communicator.BioFeedbackServiceActions.ACTION_START);
		filter.addAction(Communicator.BioFeedbackServiceActions.ACTION_STOP);

		LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
		localBroadcastManager.registerReceiver(myReceiver, filter);

		Communicator.initialize(localBroadcastManager);

		Intent intent = new Intent(ExerciseService.ACTION_EXERCISE_SERVICE);
		intent.putExtra(ExerciseService.EXERCISE_TYPE, ExerciseService.DYNAMIC_EXERCISE);
		startService(intent);

		Intent intent2 = new Intent(BioFeedbackService.ACTION_BIO_FEEDBACK_SERVICE);
		startService(intent2);

	}

	@Override
	public synchronized void onResume() {
		Log.d(TAG, "+ ON RESUME +");
		super.onResume();
	}

	@Override
	public void onDestroy() {
		LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver);
		Log.d(TAG, "--- ON DESTROY ---");
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.option_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.scan:
			enableBlueTooth();
			// Launch the DeviceListAncctivity to see devices and do scan
			Intent serverIntent = new Intent(this, DeviceListActivity.class);
			startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
			return true;
		case R.id.discoverable:
			ensureDiscoverable();
			return true;
		case R.id.menu_save:
			showSaveDialog();
			return true;
		case R.id.menu_load:
			showLoadDialog();
			return true;
		case R.id.menu_delete:
			showDeleteDialog();
		case R.id.menu_dynamic_exercise:
			dynamicExercise = new DynamicExercise();
			// setContentView(R.layout.main_dynamic_exercise);
			Intent intent1 = new Intent(ExerciseService.ACTION_EXERCISE_SERVICE);
			intent1.putExtra(ExerciseService.EXERCISE_TYPE, ExerciseService.DYNAMIC_EXERCISE);
			startService(intent1);
			// initUIs();
			return true;
		case R.id.menu_isometric_exercise:
			isometricExercise = new IsometricExercise();
			// setContentView(R.layout.main_isometric_exercise);
			Intent intent2 = new Intent(ExerciseService.ACTION_EXERCISE_SERVICE);
			intent2.putExtra(ExerciseService.EXERCISE_TYPE, ExerciseService.ISOMETRIC_EXERCISE);
			startService(intent2);
			// initUIs();
			return true;
		}
		return false;
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d(TAG, "onActivityResult " + resultCode);
		switch (requestCode) {
		case REQUEST_CONNECT_DEVICE:
			if (resultCode == Activity.RESULT_OK) {
				String address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
				BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
				try {
					Intent intent = new Intent(BluetoothService.ACTION_CONNECT_TO_DEVICE);
					intent.putExtra(BluetoothService.KEY_DEVICE_TO_CONNECT, device);
					LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
				} catch (Exception e) {
					Log.d(TAG, "onActivityResult got an error!", e);
				}
			}
			break;
		case REQUEST_ENABLE_BT:
			if (resultCode == Activity.RESULT_OK) {
				Intent intent = new Intent(BluetoothService.ACTION_BLUETOOTH_SERVICE);
				startService(intent);
			} else {
				// User did not enable Bluetooth or an error occured
				Log.d(TAG, "BlueTooth not enabled " + resultCode);

				Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
			}
		}
	}

	/********************************** OTHER METHODS ***********************************/

	// TODO remove this method and its menu button when Bluetooth communication
	// is done
	private void ensureDiscoverable() {

		Log.d(TAG, "ensure discoverable ");

		if (mBluetoothAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
			Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
			discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
			startActivity(discoverableIntent);
		}
	}

	private void enableBlueTooth() {
		if (!mBluetoothAdapter.isEnabled()) { // Enable bluetooth and start
												// bluetooth service
			Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
		} else { // Bluetooth already enabled, just start the service
			Intent intent = new Intent(BluetoothService.ACTION_BLUETOOTH_SERVICE);
			startService(intent);
		}
	}

	/********************************** PRIVATE CLASSES ***********************************/

	private class mReceiver extends BroadcastReceiver {

		// TODO ajust
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();

			if (action.equals(BluetoothService.ACTION_DATA_RECEIVED)) {
				TextView test = (TextView) findViewById(R.id.text_real_value);
				test.setTextColor(Color.RED);
				test.setText(String.valueOf(intent.getExtras().getFloat(BluetoothService.KEY_DATA_RECEIVED)));

			} else if (action.equals(BluetoothService.ACTION_MESSAGE_DEVICE_NAME)) {
				// save the connected device's name
				mConnectedDeviceName = intent.getExtras().getString(BluetoothService.KEY_DEVICE_NAME);
				Toast.makeText(getApplicationContext(), "Connected to " + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
			} else if (action.equals(BluetoothService.ACTION_MESSAGE_SHOW_TOAST)) {
				Toast.makeText(getApplicationContext(), intent.getExtras().getString(BluetoothService.KEY_TOAST_MESSAGE),
						Toast.LENGTH_SHORT).show();
			} else if (action.equals(Communicator.UIActions.ACTION_EXERCISE_FINISHED)) {
				showFinishedExcercise();
				if (restTimer != null)
					restTimer.cancel();
				textTimer.setText("STOP");
				barTimer.setProgress(100);
			} else if (action.equals(Communicator.UIActions.ACTION_START_REST_TIMER)) {
				if (restTimer != null)
					restTimer.cancel();
				restTimer = new RestTimer(intent.getExtras().getLong(Communicator.KEY_REST_TIME));
				restTimer.start();
			} else if (action.equals(Communicator.UIActions.ACTION_STOP_REST_TIMER)) {
				if (restTimer != null)
					restTimer.cancel();
			} else if (action.equals(Communicator.UIActions.ACTION_UPDATE_REPETITIONS)) {
				repetitions_picker.setValue(intent.getExtras().getInt(Communicator.KEY_REPETITIONS));
			} else if (action.equals(Communicator.UIActions.ACTION_UPDATE_SERIES)) {
				series_picker.setValue(intent.getExtras().getInt(Communicator.KEY_SERIES));
			} else if (action.equals(Communicator.UIActions.ACTION_UPDATE_LIMITS)) {
				top_limit_picker.setValue((int) intent.getExtras().getFloat(Communicator.KEY_TOP_LIMIT));
				bottom_limit_picker.setValue((int) intent.getExtras().getFloat(Communicator.KEY_BOTTOM_LIMIT));
			}
		}
	}

	private class RestTimer extends CountDownTimer {
		private long duration=30000;

		public RestTimer(long duration) {
			super(duration, 500);
			this.duration = duration;
		}

		@Override
		public void onTick(long millisUntilFinished) {
			long seconds = millisUntilFinished / 1000;
			double progressPercent = ((double) millisUntilFinished) / ((double) duration);
			int number = (int) (progressPercent * 100);
			barTimer.setProgress(number);
			textTimer.setText(String.format("%02d", seconds / 60) + ":" + String.format("%02d", seconds % 60));
		}

		@Override
		public void onFinish() {
			textTimer.setText("STOP");
			barTimer.setProgress(100);
		}
		
		public long getDuration(){
			return duration;
		}
	}

	private void showFinishedExcercise() {
		LayoutInflater li = getLayoutInflater();
		View view = li.inflate(R.layout.alert_dialog_finished_exercise, null);
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Parabéns! Concluiu o exercício!");
		builder.setView(view);
		AlertDialog alerta = builder.create();
		alerta.show();
	}

	private void saveData(String name) {
		float maximum = top_limit_picker.getValue();
		float minimum = bottom_limit_picker.getValue();
		int series = series_picker.getValue();
		int repetitions = repetitions_picker.getValue();
		
		long restTime = restTimer!=null?restTimer.getDuration():DEFAULT_DURATION;

		// TODO implement holdTimer UI. For now we'll send restTime duration
		long holdTime = restTime;
		ExerciseData exerciseData = new ExerciseData(maximum, minimum, repetitions, series, restTime, holdTime);
		exerciseData.setName(name);
		ExerciseDAO.getInstance(this).open();
		ExerciseDAO.getInstance(this).insertExerciseData(name, exerciseData);
		ExerciseDAO.getInstance(this).close();
	}

	private void showSaveDialog() {
		EditText input = new EditText(this);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.MATCH_PARENT);
		input.setLayoutParams(lp);
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Name of this setup:");
		builder.setView(input);
		builder.setPositiveButton("Save", new SaveListener(input));
		AlertDialog alertDialog = builder.create();
		alertDialog.show();
	}

	private void showDeleteDialog() {
		ExerciseDAO.getInstance(this).open();
		List<ExerciseData> list = ExerciseDAO.getInstance(this).readAll();
		ExerciseDAO.getInstance(this).close();

		DeleteItemAdapter deleteItemAdapter = new DeleteItemAdapter(this, list);
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setTitle("Choose Exercise to delete:");
		builder.setAdapter(deleteItemAdapter, null);
		builder.setPositiveButton("Delete", deleteItemAdapter);
		builder.setNegativeButton("Cancel", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		AlertDialog alertDialog = builder.create();
		alertDialog.getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		alertDialog.getListView().setItemsCanFocus(false);
		alertDialog.show();
	}

	private void showLoadDialog() {
		ExerciseDAO.getInstance(this).open();
		List<ExerciseData> list = ExerciseDAO.getInstance(this).readAll();
		ExerciseDAO.getInstance(this).close();

		LoadItemAdapter loadItemAdapter = new LoadItemAdapter(this, list);
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Choose Exercise to load:");
		builder.setAdapter(loadItemAdapter, null);
		AlertDialog alertDialog = builder.create();
		alertDialog.getListView().setOnItemClickListener(loadItemAdapter);
		alertDialog.getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		alertDialog.getListView().setItemsCanFocus(false);
		alertDialog.show();
		loadItemAdapter.setCalledAlertDialog(alertDialog);
	}

	private void showRepeatedNamePreferenceDialog(String typeException) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		if (typeException == "IllegalArgumentException") {
			builder.setTitle("You can't save an exercise whithout name!");
		}
		if (typeException == "SQLiteConstraintException") {
			builder.setTitle("This name already exist! Try another.");
		}

		builder.setPositiveButton("OK", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				showSaveDialog();
			}
		});
		AlertDialog alertDialog = builder.create();
		alertDialog.show();
	}

	private class SaveListener implements OnClickListener {
		EditText nameEditText;

		public SaveListener(EditText nameEditText) {
			this.nameEditText = nameEditText;
		}

		@Override
		public void onClick(DialogInterface dialog, int which) throws IllegalArgumentException {
			try {
				String name = nameEditText.getText().toString();
				if (name.equals("")) {
					throw new IllegalArgumentException();
				} else
					saveData(name);
			} catch (SQLiteConstraintException e) {
				showRepeatedNamePreferenceDialog("SQLiteConstraintException");
			} catch (IllegalArgumentException e) {
				showRepeatedNamePreferenceDialog("IllegalArgumentException");
			}
		}
	}
}