require 'calabash-android/calabash_steps'

Then /^I press view with id "([^\"]*)" in counter_ui with id "([^\"]*)"$/ do |view_id, counter_id|
 	tap_when_element_exists("* id:'#{counter_id}' Button id:'#{view_id}'")
<<<<<<< HEAD
end
=======
end

Then /^I set value "([^\"]*)" into counter_ui with id "([^\"]*)"$/ do |value, counter_id|
	query("* id:'#{counter_id}'", "setValue" => value.to_i)
end

Then /^I should see "([^\"]*)" into counter_ui with id "([^\"]*)"$/ do |text, counter_id|
	if text != query("* id:'#{counter_id}' TextView", "getText").first
		raise "Text not founded in CounterUI"
	end
end
>>>>>>> master
